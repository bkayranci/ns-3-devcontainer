FROM mcr.microsoft.com/vscode/devcontainers/base:focal

RUN apt update -y && apt install -y g++ python3 cmake git
RUN mkdir -p /root/repos
RUN git clone https://gitlab.com/nsnam/ns-3-allinone.git /root/repos/ns-3-allinone
RUN cd /root/repos/ns-3-allinone; python3 ./download.py -n ns-3.35
WORKDIR /root/repos/ns-3-allinone/ns-3.35

RUN ./waf --disable-python configure
RUN ./waf --disable-python build
